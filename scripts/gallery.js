/*jshint esversion: 6, browser: true, -W117, -W097 */
'use strict';
// for best minification.
// npm install -g babel-minify --save-dev
if(!window.gsap){
    let script2 = document.createElement("script");
    script2.type = "text/javascript";
    script2.src = "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js";
    document.getElementsByTagName("head")[0].appendChild(script2);
}

function print(words){
    console.log(words);
}

function mkGallery(self, configFile, setCtrl, verbose){

    var loc, tLoc, btn_bg, btnH, btnW, btnS, fsb, objct, info, iGallery, iKey, gIndex,
        pKey, folderBG, ellipse, showInfo, isFS, metaScale, wW, wH, fsWidth, fsHeight,
        direction, galleryScrollTimeout;

    btnS = [];
    showInfo=false;
    // fullscreen variables
    isFS = false; // is fullscreen
    fsWidth = 1;  // fullscreen image width
    fsHeight = 1; // fullscreen image height

    let vp = '"viewport"';

    metaScale = document.querySelector('meta[name='+ vp +']').content;

    setTimeout(null, 300);
    wW = screen.width || document.documentElement.clientWidth || document.body.clientWidth;
    wH = screen.height || document.documentElement.clientHeight || document.body.clientHeight;

    function mkInt(str){
        str = str.replace("px", "").replace("%", ""); 
        return Number(str);
    }

    function mkLog(txt){
        if(verbose){
            console.log(txt);
        }
    }

    function getTopDir(){
        if(setCtrl.topDir === ''){
            if(objct.settings.topDir === ''){
                return '';
            }else{
                return objct.settings.topDir;
            }
        }else{
            return setCtrl.topDir;
        }
    }

    function mkPath(pth){
        if(tLoc === ''){
            return  pth;
        }
        else{
            return tLoc+'/'+pth;
        }
    }

    function strFix(fStr){
        // Not used anywhere yet
        return fStr.replace(/_/g, ' ');
    }

    function strUnFix(fStr){
        return fStr.replace(/ /g, '_');
    }

    function scrolToImg(){
        let thumb = document.getElementById('thumb_'+gIndex).parentNode;
        let offset = mkInt(document.getElementById('thumbScroller').style.width) * 0.5;
        let scroller = document.getElementById('thumbScroller');
        scroller.scrollTo((mkInt(thumb.style.left) - offset) + (mkInt(thumb.children[0].style.width) * 0.5), 0);
    }

    function btnOver(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        btN = bt.id.replace('Top', '_btn');
        btN = document.getElementById(btN);
        gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
    }

    function btnOut(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        btN = bt.id.replace('Top', '_btn');
        btN = document.getElementById(btN);
        gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0, overwrite: true});
    }

    function chBtnBgColor(topColor, topAlpha, bottomColor, bottomAlpha){
        gsap.to(bottomFill, {duration: 0, fill: topColor, fillOpacity: topAlpha, overwrite: true});
        gsap.to(topFill, {duration: 0, fill: bottomColor, fillOpacity: bottomAlpha, overwrite: true});
    }

    function chBtnBgAlpha(topAlpha, bottomAlpha){
        gsap.to(bottomFill, {duration: 0, fillOpacity: topAlpha, overwrite: true});
        gsap.to(topFill, {duration: 0, fillOpacity: bottomAlpha, overwrite: true});
    }

    function hexToRgb(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    function isDark(color, threshold){
        let colorObj, red, green, blue, num;
        if(threshold == undefined || threshold < 2 || threshold > 255){
            threshold = 111;
        }
        colorObj = hexToRgb(color.replace('#', ''));
        red = colorObj.r;
        green = colorObj.g;
        blue = colorObj.b;
        num = ((red * 299) + (green * 587) + (blue * 114)) / 1000;

        if(num >= threshold){
            return false;
        }else{
            return true;
        }
    }

    function imgChanger(e){
        let tgt = e.target || e.srcElement || window.event;
        let pName = tgt.id.replace('Top', '');
        let stage = document.getElementById('stage');

        if(pName == 'gallery-minus'){
            direction = 'left';
            imgTrac(stage);
        }else{
            direction = 'right';
            imgTrac(stage);
        }
    }

    function imgTrac(stage){
        let tW = mkInt(stage.style.width);
        let tH = mkInt(stage.style.height);
        let iDiv = stage.children[0];
        let img = iDiv.children[0];
        // let time = 1;
        let imgcount = iGallery.images.length;
        let upDown;

        if(isFS){
            upDown = [Math.floor((tW - fsWidth) * 0.5),
                      Math.ceil((tW - fsWidth) * 0.5),
                      Math.floor((tW - iGallery.images[gIndex].width) * 0.5),
                      Math.ceil((tW - iGallery.images[gIndex].width) * 0.5), 0];
        }else{
            upDown = [Math.floor((tW - iGallery.images[gIndex].width) * 0.5),
                      Math.ceil((tW - iGallery.images[gIndex].width) * 0.5), 0];
        }

        let scroller = document.getElementById('thumbScroller');

        if(direction == 'right'){
            // the minus function.
            if(upDown.indexOf(Math.round(mkInt(iDiv.style.left))) != -1){

                gIndex = gIndex + 1;

                if(gIndex < 0){
                    gIndex = imgcount - 1;
                }else if(gIndex >= imgcount){
                    gIndex = 0;
                }
 
                gsap.to(iDiv, {
                            left: -Math.abs(mkInt(iDiv.style.width) + 10) + 'px',
                            duration: 1,
                            ease: Power2.easeOut,
                            onComplete: function(){
                                iDiv.style.visibility = 'hidden';
                                iDiv.style.left = tW + 2 +'px';
                                img.style.visibility = 'hidden';
                                image_getter();
                                getImageInfo();
                            }
                });
            }
        }
        if(direction == 'left'){
            if(upDown.indexOf(Math.round(mkInt(iDiv.style.left))) != -1){

                gIndex = gIndex - 1;

                if(gIndex < 0){
                    gIndex = imgcount - 1;
                }else if(gIndex >= imgcount){
                    gIndex = 0;
                }
                
                gsap.to(iDiv, {
                            left: tW + 10 + 'px',
                            duration: 1,
                            ease: Power2.easeOut,
                            onComplete: function(){
                                iDiv.style.visibility = 'hidden';
                                iDiv.style.left = -Math.abs(mkInt(iDiv.style.width) + 10) + 'px';
                                img.style.visibility = 'hidden';
                                image_getter();
                                getImageInfo();
                            }
                });
            }
        }
        scrolToImg();
    }

    function thumbnailer(gallery){
        let rightButton = document.getElementById('gallery-fwdBtn');
        let infoButton = document.getElementById('gallery-infoBtn');
        let thumbHolder = document.getElementById('thumbScroller');
        let scrolFind = document.getElementById('scrollFinder');

        if(thumbHolder === undefined || thumbHolder === null){
            thumbHolder = document.createElement('div');
            thumbHolder.id = 'thumbScroller';
            thumbHolder.style.position = 'absolute';
            thumbHolder.style.overflow = 'hidden';
            thumbHolder.style.height = objct.settings.thumb_size[1] + 10 + 'px';
            thumbHolder.style.zIndex = mkInt(self.style.zIndex) + 2;
            self.appendChild(thumbHolder);
        }else{
            thumbHolder.innerHTML = '';
        }

        thumbHolder.style.right = mkInt(infoButton.style.right) + mkInt(infoButton.style.width) + 4 + 'px';

        if(scrolFind !== null){
            self.removeChild(scrolFind);
        }

        if(isFS){
            thumbHolder.style.width = (mkInt(self.style.width) * 0.32) + 'px';
            thumbHolder.style.bottom = 0 + 'px';
        }
        else{
            thumbHolder.style.width = mkInt(self.style.width) - (mkInt(rightButton.style.width) * 3.75) - mkInt(infoButton.style.left) - 4 + 'px';
            thumbHolder.style.bottom = -Math.abs(objct.settings.thumb_size[1]) - 15 + 'px';
            
            scrolFind = thumbHolder.cloneNode(true);
            scrolFind.id = 'scrollFinder';
            scrolFind.style.height = mkInt(thumbHolder.style.height) + 10 + 'px';
            scrolFind.style.zIndex = mkInt(self.style.zIndex) + 1;
            scrolFind.style.right = mkInt(thumbHolder.style.right) - 20 + 'px';
            scrolFind.style.opacity = 0.4;
            scrolFind.style.width = mkInt(thumbHolder.style.width) + 40 + 'px';
            scrolFind.style.bottom = mkInt(thumbHolder.style.bottom) - 10 + 'px';
            
            self.appendChild(scrolFind);

            scrolFind.addEventListener('mouseover', function(){
                clearTimeout(galleryScrollTimeout);
            }, false);

            scrolFind.addEventListener('mouseout', function(){
                clearTimeout(galleryScrollTimeout);
                galleryScrollTimeout = setTimeout(scrolToImg, 3 * 1000);
            }, false);
        }
        
        let lefty = 6;

        for(let imgIndex in iGallery.images){

            let imgObj = iGallery.images[imgIndex];
            let thumb = document.createElement('div');

            thumb.style.position = 'absolute';
            thumb.style.bottom = (objct.settings.thumb_size[1] - imgObj.thumb.height) * 0.5 + 'px';
            thumb.style.width = imgObj.thumb.width + 'px';
            thumb.style.height = imgObj.thumb.height + 'px';
            thumb.style.zIndex = mkInt(self.style.zIndex) + 3;

            if(imgIndex === '0'){
                thumb.style.left = lefty + 'px';
            }else{
                lefty = lefty + iGallery.images[imgIndex -1].thumb.width + 3;
                thumb.style.left = lefty + 'px';
            }
            
            thumbHolder.appendChild(thumb);

            let thumbImg = document.createElement('img');
            thumbImg.id = 'thumb_'+imgIndex;
            thumbImg.style.width = imgObj.thumb.width + 'px';
            thumbImg.style.height = imgObj.thumb.height + 'px';
            thumbImg.style.bottom = '0px';
            thumbImg.style.left = '0px';
            thumbImg.src = mkPath(imgObj.thumb.path);

            thumb.appendChild(thumbImg);

            thumbImg.addEventListener('load', function(){
                thumbImg.style.visibility = 'visible';
                gsap.from(thumbImg, {scaleX: 0,
                                     scaleY: 0,
                                     opacity: 0,
                                     ease: Power2.easeOut,
                                     duration: 0.5});
            });

            thumb.addEventListener('mouseover', thumbOver, false);
        }

        let scruBar = document.getElementById('galleryScrubber');

        if(scruBar === null || scruBar === undefined){
            scruBar = document.createElement('div');
            scruBar.id = 'galleryScrubber';
            scruBar.style.position = 'absolute';
            scruBar.style.cursor='pointer';
            scruBar.style.height = mkInt(rightButton.style.height) + 'px';
        }

        if(isFS){
            scruBar.style.width = (mkInt(self.style.width) * 0.33) + (mkInt(btnW) * 3) + 'px';
            scruBar.style.bottom = objct.settings.thumb_size[1] + 'px';
        }else{
            scruBar.style.width = self.style.width;
            scruBar.style.bottom = '0px';
        }

        scruBar.style.right = rightButton.style.right;
        scruBar.style.zIndex = mkInt(self.style.zIndex) + 2;

        self.appendChild(scruBar);

        scruBar.addEventListener('mousemove', scroller, false);

        scruBar.addEventListener('mouseout', function(){
            clearTimeout(galleryScrollTimeout);
            galleryScrollTimeout = setTimeout(scrolToImg, 3.2 * 1000);
        }, false);

        scruBar.addEventListener('mouseover', function(){
            clearTimeout(galleryScrollTimeout);
        }, false);
    }

    function thumbClick(e){
        let tgt = e.target || e.srcElement || window.event;
        let stage = document.getElementById('stage');
        let tW = mkInt(stage.style.width);
        let tH = mkInt(stage.style.height);
        let iDiv = stage.children[0];
        let img = iDiv.children[0];
        let time = 1;
        let upDown;

        if(isFS){
            upDown = [Math.floor((tW - fsWidth) * 0.5),
                      Math.ceil((tW - fsWidth) * 0.5),
                      Math.floor((tW - iGallery.images[gIndex].width) * 0.5),
                      Math.ceil((tW - iGallery.images[gIndex].width) * 0.5), 0];
        }else{
            upDown = [Math.floor((tW - iGallery.images[gIndex].width) * 0.5),
                      Math.ceil((tW - iGallery.images[gIndex].width) * 0.5), 0];
        }

        gIndex = mkInt(tgt.id.replace('thumb_', ''));

        if(upDown.indexOf(Math.round(mkInt(iDiv.style.left))) != -1){
            gsap.to(iDiv, {
                        scaleX: '0',
                        scaleY: '0',
                        duration: time,
                        ease: Power2.easeOut,
                        onComplete: function(){
                            direction = 'zoom';
                            image_getter();
                            getImageInfo();
                        }
            });
        }
    }

    function thumbOver(e){
        clearTimeout(galleryScrollTimeout);
        let tgt = e.target || e.srcElement || window.event;
        tgt.removeEventListener('mouseover', thumbOver, false);
        tgt.addEventListener('click', thumbClick, false);
        tgt.style.cursor = 'pointer';
    }

    function thumbOut(e){
        let tgt = e.target || e.srcElement || window.event;
        tgt.addEventListener('mouseover', thumbOver, false);
        tgt.removeEventListener('click', thumbClick, false);
        tgt.style.cursor = 'auto';
    }

    function scroller(e){
        let tgt = e.target || e.srcElement || window.event;
        let scrol = document.getElementById('thumbScroller');

        let tWidth = mkInt(tgt.style.width);
        let sWidth = scrol.scrollWidth;
        let maskW = mkInt(scrol.style.width);

        let maxW = sWidth - maskW;
        // Here we need to add the difference of the mask and the thumb scrubber 
        // to get the correct scrub ratio. Only maginally off up into the hundreds
        // which is probably ok for the purposes this gallery would likely serve
        let ratio = maxW / (tWidth + (tWidth - maskW));
        let relX = (e.offsetX * ratio) * (tWidth / maskW);
        scrol.scrollTo(relX, 0);

        // gsap.to(scrol, 
        //         {duration: 0.2,
        //          scrollTo: relX}
        // );
    }

    function getImageInfo(){
        let infoDiv = document.getElementById('gallery-image-info');
        infoDiv.innerHTML = '';
        infoDiv.style.display = 'inline-block';

        let imageName = document.createElement('div');
        imageName.style.position = 'absolute';
        imageName.style.position = 'relative';
        imageName.style.margin = '0px 3px 1px 0px';
        imageName.style.width = mkInt(infoDiv.style.width) * 0.96 + 'px';
        imageName.style.height = mkInt(infoDiv.style.height) * 0.15 + 'px';
        imageName.style.top = (mkInt(infoDiv.style.height) * 0.02) + 'px';
        imageName.style.left = mkInt(infoDiv.style.width) * 0.02 + 'px';
        
        infoDiv.appendChild(imageName);

        let name = document.createElement('h3');
        name.className = 'gallery-title';
        name.style.color = setCtrl.folder.folderTab;
        if(isDark(setCtrl.folder.folderTab, 111)){
            name.style.textShadow = "0px 0px 20px #ffffff";
        }else{
            name.style.textShadow = "0px 0px 20px #000000";
        }
        name.style.textAlign = 'center';
        name.style.padding = '0px 3px 3px 3px';
        name.innerHTML = strFix(iGallery.images[gIndex].name);
        // name.style.fontSize = '16px';
        
        imageName.appendChild(name);

        let imageTxt = document.createElement('div');
        imageTxt.style.position = 'relative';
        imageTxt.style.margin = '0px 3px 1px 0px';
        imageTxt.style.overflow = 'auto';
        imageTxt.style.left = imageName.style.left;
        imageTxt.style.width = imageName.style.width;
        imageTxt.style.height = mkInt(infoDiv.style.height) * 0.80 + 'px';
        imageTxt.style.bottom = mkInt(infoDiv.style.height) * 0.02 + 'px';

        infoDiv.appendChild(imageTxt);

        let img = document.getElementById('thumb_'+gIndex).cloneNode(true);
        img.id = 'infoPic';
        img.style.position = 'relative';
        img.style.visibility = 'visible';
        img.style.left = '0px';
        img.style.top = '0px';
        img.style.float = 'left';
        img.style.margin = '10px 10px 10px 10px';

        imageTxt.appendChild(img);

        let text = iGallery.images[gIndex].info_text.split('\n');

        for(let parg in text){
            let pTag = document.createElement('p');
            pTag.className = 'gallery-paragraph';
            pTag.style.position = 'relative';
            pTag.style.width = mkInt(imageTxt.style.width) * 0.93 + 'px';
            pTag.innerHTML = text[parg];
            pTag.style.textIndent = '20px';
            pTag.style.color = '#FFFFFF';
            pTag.style.display = 'block';
            pTag.style.padding = '2px 2px 2px 2px';
            imageTxt.appendChild(pTag);
        }
    }

    function mkFolder(gObj, gName){
        let folder_bg = folderBG.cloneNode(true);

        let folder = document.createElement('div');
        // folder.id = gName;
        folder.style.position = 'absolute';
        folder.style.top = '0px';
        folder.style.left = '0px';
        folder.style.width = objct.settings.thumb_size[0] * 1.25 + 'px';
        folder.style.height = objct.settings.thumb_size[1] * 1.25 + 'px';
        folder.appendChild(folder_bg);

        let img = document.createElement('img');
        img.style.position = 'absolute';
        folder.appendChild(img);
        
        let tHeight, tWidth, imgSrc;

        if(gObj.galleryIndex === undefined || typeof gObj.galleryIndex  != 'number'){
            tHeight = gObj.images[0].thumb.height;
            tWidth = gObj.images[0].thumb.width;
            imgSrc = gObj.images[0].thumb.path;
        }else{
            tHeight = gObj.images[gObj.galleryIndex].thumb.height;
            tWidth = gObj.images[gObj.galleryIndex].thumb.width;
            imgSrc = gObj.images[gObj.galleryIndex].thumb.path;
        }

        img.src = mkPath(imgSrc);
        img.addEventListener('load', function(){
            if(tWidth > tHeight){
                img.style.top = (mkInt(folder.style.height) - tHeight) * 0.6 + 'px';
                img.style.left = (mkInt(folder.style.width) - tWidth) * 0.30 + 'px';
                img.style.transform = "rotate(-6deg)";
            }else{
                img.style.top = (mkInt(folder.style.height) - tHeight) * 0.70 + 'px';
                img.style.left = (mkInt(folder.style.width) - tWidth) * 0.39 + 'px';
                img.style.transform = "rotate(87deg)";
            }

            img.style.visibility = 'visible';
            gsap.from(img, {
                scaleX: 0,
                scaleY: 0,
                duration: 0.33,
                ease: Power2.easeIn
            });
        });

        let blank = folder.cloneNode(true);
        blank.innerHTML = '';
        // blank.id = 'blank';
        folder.appendChild(blank);
        
        return folder;
    }

    function getGalleryInfo(tGallery, tKey){
        let infoDiv = document.getElementById('gallery-info');

        let maxW = mkInt(infoDiv.style.width), maxH = mkInt(infoDiv.style.height);
        
        gsap.to(infoDiv, {
                            width: '0px',
                            height: '0px',
                            scaleX: 0,
                            scaleY: 0,
                            duration: 0.2,
                            overwrite: true,
                            ease: Power2.easeIn
                         }
        );

        infoDiv.innerHTML = '';

        let infoHolder = document.createElement('div');
        infoHolder.style.position = 'absolute';
        infoHolder.style.overflow = 'auto';
        infoHolder.style.display = 'inline-block';
        infoHolder.style.width = mkInt(infoDiv.style.width) * 0.98 + 'px';
        infoHolder.style.left = (mkInt(infoDiv.style.width) - mkInt(infoHolder.style.width)) * 0.5 + 'px';
        infoHolder.style.height = infoDiv.style.height;
        infoDiv.appendChild(infoHolder);

        let galImg = mkFolder(tGallery, tKey);
        galImg.id = tKey+'info';
        galImg.style.position = 'relative';
        galImg.style.overflow = 'hidden';
        galImg.style.float = 'left';
        galImg.style.left = '0px';
        galImg.style.top = '0px';
        galImg.style.margin = '0px 3px 1px 0px';
        infoHolder.appendChild(galImg);

        let galH = document.createElement('h3');
        galH.className = 'gallery-title';
        galH.style.position = 'relative';
        galH.style.display = 'inling-block';
        galH.style.padding = '0px 0px 1px 0px';
        galH.style.textAlign = 'center';
        galH.innerHTML = strFix(tKey);
        // galH.style.fontSize = '16px';
        galH.style.color = setCtrl.folder.folderTab;
        if(isDark(setCtrl.folder.folderTab, 111)){
            galH.style.textShadow = "0px 0px 20px #ffffff";
        }else{
            galH.style.textShadow = "0px 0px 20px #000000";
        }

        infoHolder.appendChild(galH);

        let words = tGallery.info.split('\n');
        
        for(let word in words){
            let pTag = document.createElement('p');
            pTag.className = 'gallery-paragraph';
            pTag.style.display = 'block';
            pTag.innerHTML = words[word];
            pTag.style.padding = '1px 2px 2px 2px';
            pTag.style.textIndent = '20px';
            pTag.style.color = '#FFFFFF';
            infoHolder.appendChild(pTag);
        }

        gsap.to(infoDiv, {
                          scaleX: 1,
                          scaleY: 1,
                          width: maxW + 'px',
                          height: maxH + 'px',
                          duration: 0.2,
                          overwrite: true,
                          ease: Power2.easeIn
                         }
        );
    }

    function galleryOver(e){
        let tgt = e.target || e.srcElement || window.event;
        let oGallery = tgt.parentNode.id;
        let folder = document.getElementById(oGallery);

        folder.removeEventListener('mouseover', galleryOver, false);
        folder.addEventListener('click', gallerySelect, false);
        folder.addEventListener('mouseout', galleryOut, false);

        if(tgt.parentNode.id !== iKey){
           getGalleryInfo(objct.gallerys[oGallery], oGallery);
        }
    }

    function galleryOut(e){
        let tgt = e.target || e.srcElement || window.event;
        let folder = document.getElementById(tgt.parentNode.id);

        folder.addEventListener('mouseover', galleryOver, false);
        folder.removeEventListener('click', gallerySelect, false);
        folder.removeEventListener('mouseout', galleryOut, false);

        if(tgt.parentNode.id !== iKey){
            getGalleryInfo(iGallery, iKey);
        }
    }

    function gallerySelect(e){
        let tgt = e.target || e.srcElement || window.event;
        let oGallery = tgt.parentNode.id;

        iGallery = objct.gallerys[oGallery];
        iKey = oGallery;

        getGalleryInfo(iGallery, iKey);
        // cereate our thumbnails, their listeners and scrub-bar
        thumbnailer(iGallery);

        if(iGallery.galleryIndex === undefined){
            gIndex = 0;
        }else{
            gIndex = iGallery.galleryIndex;
        }

        let stage = document.getElementById('stage');
        let tW = mkInt(stage.style.width);
        let tH = mkInt(stage.style.height);
        let iDiv = stage.children[0];
        let img = iDiv.children[0];
        let time = 1;

        gsap.to(iDiv, {
                scaleX: '0',
                scaleY: '0',
                duration: time,
                ease: Power2.easeOut,
                onComplete: function(){
                    direction = 'zoom';
                    image_getter();
                    getImageInfo();
                }
        });
    }

    function infoSplash(){
        // builds the image/gallery info splash changer.

        let maxW, maxH, iBottom, iLeft;

        let splash = document.getElementById('gallery-backsplash');

        if(splash !== undefined && splash !== null){
            self.removeChild(splash);
        }

        if(isFS){
            maxW = mkInt(self.style.width) * 0.5;
            maxH = mkInt(self.style.height) * 0.5;
            iBottom = objct.settings.thumb_size[1] + 'px';
            iLeft = (mkInt(self.style.width) - maxW) * 0.5 + 'px';
        }else{
            maxW = mkInt(self.style.width);
            maxH = mkInt(self.style.height);
            iBottom = '0px';
            iLeft = '0px';
        }


        splash = document.createElement('div');
        splash.style.position = 'absolute';
        splash.id = 'gallery-backsplash';
        splash.style.overflow = 'hidden';
        splash.style.zIndex = mkInt(self.style.zIndex) + 5;
        splash.style.bottom = iBottom;
        splash.style.left = iLeft;
        splash.style.width = maxW + 'px';
        splash.style.height = maxH + 'px';

        self.appendChild(splash);
        
        let blank = splash.cloneNode(true);
        blank.id = 'gallery-bgChanger';
        blank.style.backgroundColor = '#000000';
        blank.style.opacity = 0.6;
        blank.style.top = '0px';
        blank.style.left = '0px';

        splash.appendChild(blank);
        splash.style.height = 0 + 'px';

        let imageInfo = document.createElement('div');
        imageInfo.id = 'gallery-image-info';
        imageInfo.style.position = 'absolute';
        imageInfo.style.display = 'inline-block';
        imageInfo.style.zIndex = mkInt(splash.style.zIndex) + 3;
        imageInfo.style.top = '0px';
        imageInfo.style.left = '0px';
        imageInfo.style.width = maxW * 0.48 + 'px';
        imageInfo.style.height = maxH * 0.96 + 'px';
        imageInfo.style.opacity = 0.85;

        splash.appendChild(imageInfo);
        
        getImageInfo();

        let galleryInfo = imageInfo.cloneNode(true);
        galleryInfo.id = 'gallery-info';
        galleryInfo.style.position = 'absolute';
        galleryInfo.style.top = '5px';
        galleryInfo.style.left = (maxW * 0.5) + ((maxW * 0.5) - (maxW * 0.48) * 0.8) * 0.1 + 'px';
        galleryInfo.style.width = mkInt(galleryInfo.style.width) + 5 + 'px';

        splash.appendChild(galleryInfo);

        let galPic = document.createElement('div');
        galPic.id = 'gallery-picker';
        galPic.style.position = 'absolute';
        galPic.style.overflow = 'auto';
        galPic.style.zIndex = mkInt(splash.style.zIndex) + 2;
        galPic.style.bottom = '0px';
        galPic.style.left = mkInt(galleryInfo.style.left) - ((mkInt(galleryInfo.style.width) * 1.07) - mkInt(galleryInfo.style.width)) + 'px';
        galPic.style.width = mkInt(galleryInfo.style.width) * 1.06 + 'px';
        galPic.style.opacity = 0;

        splash.appendChild(galPic);

        let galSplat = document.createElement('div');
        galSplat.style.position = 'absolute';
        galPic.appendChild(galSplat);

        let lefty = 0, topy = 0, first = true, largest = 0, count = -1, spliter = 0;

        for(let pGal in objct.gallerys){
            count += 1;
            let folder = mkFolder(objct.gallerys[pGal], pGal);
            folder.id = pGal;
            folder.style.position = 'absolute';
            
            if(count %spliter !== 0){
                lefty = lefty + mkInt(folder.style.width);
                if(largest < lefty)largest=lefty;
            }else{
                topy = topy + mkInt(folder.style.height);
                lefty = 0;
            }

            if(first){
                // first run so we need to set our initial values left and top
                first = false;
                lefty = 0;
                topy = 0;
                // this our first run so we need to set a bunch of things that rely on the size 
                // of our folder div.
                galPic.style.height = mkInt(folder.style.height) * 1.02 + 'px';
                galleryInfo.style.height = maxH -mkInt(galPic.style.height) - 20 + 'px';
                spliter = Math.floor(mkInt(galPic.style.width) / (mkInt(folder.style.width) + 3));
                galSplat.style.width = Math.round(mkInt(folder.style.width) * spliter) + 'px'; 
            }

            folder.style.left = lefty + 'px';
            folder.style.top = topy+'px';

            galSplat.appendChild(folder);

            folder.addEventListener('mouseover', galleryOver, false);
        }

        galSplat.style.left = (mkInt(galPic.style.width) - mkInt(galSplat.style.width)) * 0.2 + 'px';
        galSplat.style.height = galSplat.scrollHeight + 'px';
        getGalleryInfo(iGallery, iKey);
    }

    function infoShow(){
        showInfo = true;
        gsap.to(ellipse, {duration: 1.1, scaleX: 1.0, scaleY: 1.0,
                          x: 0, y: 0, overwrite: true});
        let splash = document.getElementById('gallery-backsplash');
        let blank = document.getElementById('gallery-blank');
        let gSwtcher = document.getElementById('gallery-picker');

        let maxW, maxH;

        if(isFS){
            maxH = mkInt(self.style.height) * 0.5;
        }else{
            maxH = mkInt(self.style.height);
        }

        gsap.to([splash, blank], {duration:1, height: maxH + 'px', overwrite: true});
        gsap.to(gSwtcher, {duration:1.3, delay: 0.5, opacity: 1, overwrite:true});
    }

    function infoHide(){
        showInfo = false;
        gsap.to(ellipse, {duration: 0.5, scaleX: 0.0, scaleY: 0.0,
                          x: mkInt(btnH)*0.1, y: mkInt(btnW)*0.1, overwrite: true});
        let splash = document.getElementById('gallery-backsplash');
        let gSwtcher = document.getElementById('gallery-picker');
        gsap.to(splash, {duration:1, height: 0 + 'px', overwrite: true});
        gsap.to(gSwtcher, {duration:0.3, opacity: 0, overwrite:true});
    }

    function mkButtonBG(){
        // **  Background Button SVG  ** //
        btn_bg = document.createElementNS(xmlns, 'svg');
        btn_bg.setAttribute('baseProfile', 'tiny');
        btn_bg.setAttribute('xlmns', xmlns);
        btn_bg.setAttribute('viewBox', '0 0 20 15');
        btn_bg.setAttribute('preserveAspectRatio', 'none');
        btn_bg.setAttribute('height', mkInt(btnH));
        btn_bg.setAttribute('width', mkInt(btnW));
        btn_bg.setAttribute('x', '0');
        btn_bg.setAttribute('y', '0');
        btn_bg.style.display = 'block';

        let playG = document.createElementNS(xmlns, 'g');
        playG.setAttribute('fill', setCtrl.btnBgBottom);
        btn_bg.appendChild(playG);

        let bottomFill = document.createElementNS(xmlns, 'path');
        bottomFill.setAttribute('d', 'M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z');
        playG.appendChild(bottomFill);

        let outLine = document.createElementNS(xmlns, 'path');
        outLine.setAttribute('d', 'M15.716.524c2.026 0 3.676 1.226 3.676 2.73v8.5c0 1.505-1.65 2.73-3.676 2.73H4.283c-2.026 0-3.675-1.226-3.675-2.73v-8.5c0-1.505 1.648-2.73 3.675-2.73h11.433m0-.304H4.283C2.037.22.2 1.587.2 3.257v8.498c0 1.67 1.837 3.034 4.083 3.034h11.433c2.246 0 4.084-1.37 4.084-3.04v-8.5c0-1.67-1.838-3.035-4.084-3.035z');
        playG.appendChild(outLine);

        let linGrad = document.createElementNS(xmlns, 'linearGradient');
        linGrad.setAttribute('gradientUnits', 'userSpaceOnUse');
        linGrad.setAttribute('x1', '9.999');
        linGrad.setAttribute('y1', '4.301');
        linGrad.setAttribute('x2', '9.999');
        linGrad.setAttribute('y2', '11.851');

        let stopTop = document.createElementNS(xmlns, "stop");
        stopTop.setAttribute('offset', '0');
        stopTop.setAttribute('stop-color', '#fff');
        linGrad.appendChild(stopTop);

        let stopBottom = document.createElementNS(xmlns, "stop");
        stopBottom.setAttribute('offset', '1');
        linGrad.appendChild(stopBottom);

        btn_bg.appendChild(linGrad);

        let highlightLine = document.createElementNS(xmlns, 'path');
        highlightLine.setAttribute('d', 'M15.716.828H4.282c-1.8 0-3.265 1.09-3.265 2.428v8.498c0 1.34 1.464 2.427 3.265 2.427h11.434c1.8 0 3.267-1.08 3.267-2.42v-8.5c0-1.34-1.466-2.426-3.267-2.426zm2.858 10.926c0 1.17-1.283 2.124-2.858 2.124H4.282c-1.575 0-2.857-.953-2.857-2.124V3.256c0-1.17 1.282-2.125 2.857-2.125h11.434c1.575 0 2.858.96 2.858 2.13v8.5z');
        highlightLine.setAttribute('fill', 'url(#a)');
        btn_bg.appendChild(highlightLine);

        let topFill = document.createElementNS(xmlns, 'path');
        topFill.setAttribute('d', 'M15.716.828H4.283c-1.8 0-3.267 1.09-3.267 2.428V7.65c2.72.522 5.766.815 8.984.815s6.263-.293 8.983-.814V3.26c0-1.34-1.465-2.428-3.267-2.428z');
        topFill.setAttribute('fill', setCtrl.btnBgTop);
        btn_bg.appendChild(topFill);
    }

    function mkInfoBG(){

        // **  Info Button SVG's  ** //
        info = document.createElement('div');
        info.id = 'gallery-infoBtn';
        info.style.position = 'absolute';
        info.style.width = mkInt(btnH) * 0.75 + 'px';
        info.style.height = mkInt(btnH) * 0.75 + 'px';
        info.style.zIndex = mkInt(self.style.zIndex) + 5;

        // this is for the fullscreen info button svg
        let infoBG = document.createElementNS(xmlns, 'svg');
        infoBG.setAttribute('baseProfile', 'tiny');
        infoBG.setAttribute('xlmns', xmlns);
        infoBG.setAttribute('viewBox', '0 0 20 20');
        infoBG.setAttribute('preserveAspectRatio', 'none');
        infoBG.setAttribute('height', mkInt(btnH) * 0.75);
        infoBG.setAttribute('width', mkInt(btnH) * 0.75);
        infoBG.setAttribute('x', '0');
        infoBG.setAttribute('y', '0');

        ellipse = document.createElementNS(xmlns, 'ellipse');
        ellipse.setAttribute('cx', '9.7908363');
        ellipse.setAttribute('cy', '7.3207169');
        ellipse.setAttribute('rx', '5.9063745');
        ellipse.setAttribute('ry', '5.9262953');
        ellipse.setAttribute('fill', setCtrl.btnColOver);

        gsap.to(ellipse, {duration: 0.1, scaleX: 0.0, scaleY: 0.0,
                          x: mkInt(btnH)*0.1, y: mkInt(btnW)*0.1, overwrite: true});

        let circleBG = document.createElementNS(xmlns, 'g');
        // circleBG.setAttribute('fill', setCtrl.btnBgBottom);
        infoBG.appendChild(circleBG);
        circleBG.appendChild(ellipse);

        let circleBottomFill = document.createElementNS(xmlns, 'path');
        circleBottomFill.setAttribute('d', 'm 11.281,8.145 v 2.633 c 0.125,0.072 0.377,0.23 0.377,0.49 0,0.418 -0.598,0.678 -1.415,0.678 -0.974,0 -2.294,-0.217 -2.294,-0.707 0,-0.173 0.251,-0.346 0.408,-0.403 V 8.158 C 6.77,8.102 5.235,7.968 3.766,7.774 c 0.291,3.312 2.933,5.902 6.151,5.902 3.214,0.004 5.852,-2.578 6.15,-5.883 -1.524,0.198 -3.135,0.306 -4.786,0.352 z');
        circleBottomFill.setAttribute('fill', setCtrl.btnBgBottom);
        circleBG.appendChild(circleBottomFill);

        let circleTopFill = document.createElementNS(xmlns, 'path');
        circleTopFill.setAttribute('d', 'm 9.896,1.318 c -3.402,0 -6.16,2.759 -6.16,6.165 0,0.194 0.012,0.386 0.029,0.574 1.465,0.184 3.012,0.286 4.593,0.338 V 6.711 C 8.295,6.625 8.043,6.48 8.043,6.235 8.012,5.932 8.86,5.557 9.583,5.557 c 1.918,0 1.981,0.433 1.981,0.836 0,0.13 -0.094,0.289 -0.283,0.375 V 8.406 C 12.918,8.361 14.514,8.26 16.027,8.074 16.047,7.88 16.058,7.683 16.058,7.483 16.059,4.078 13.299,1.318 9.896,1.318 Z m -0.029,3.49 c -0.975,0 -1.886,-0.49 -1.886,-1.341 0,-0.418 0.503,-0.663 1.414,-0.663 1.195,0 1.886,0.577 1.886,1.312 0,0.403 -0.408,0.692 -1.414,0.692 z');
        circleTopFill.setAttribute('fill', setCtrl.btnBgTop);
        circleBG.appendChild(circleTopFill);

        let circleBorder = document.createElementNS(xmlns, 'g');
        // circleBorder.setAttribute('fill', setCtrl.btnBgBottom);
        infoBG.appendChild(circleBorder);

        // I might want to rethink this path. The complexxity is lost on it's small size. Simplicity might be nice as well?
        let borderPath = document.createElementNS(xmlns, 'path');
        borderPath.setAttribute('d', 'M16.021,8.484c0,0-0.041,0.142-0.117,0.405    c-0.038,0.132-0.086,0.293-0.144,0.484c-0.038,0.188-0.138,0.415-0.278,0.674c-0.135,0.253-0.271,0.547-0.45,0.833    c-0.208,0.261-0.435,0.54-0.675,0.841c-0.267,0.275-0.597,0.521-0.92,0.804c-0.352,0.246-0.762,0.444-1.168,0.679    c-0.879,0.321-1.889,0.605-2.962,0.485c-0.28-0.032-0.509-0.021-0.84-0.096c-0.248-0.072-0.499-0.146-0.752-0.223    c-0.252-0.054-0.513-0.188-0.774-0.329c-0.255-0.145-0.539-0.26-0.77-0.438C5.214,11.93,4.461,10.96,3.96,9.86    C3.504,8.73,3.403,7.48,3.579,6.271c0.263-1.162,0.817-2.318,1.638-3.189C6.071,2.249,7.101,1.634,8.203,1.36    C9.3,1.15,10.43,1.118,11.398,1.479c0.492,0.102,0.93,0.38,1.357,0.603c0.428,0.231,0.748,0.58,1.104,0.847    c0.281,0.347,0.574,0.66,0.799,0.995c0.188,0.359,0.394,0.687,0.531,1.017c0.107,0.343,0.211,0.663,0.305,0.96    c0.059,0.304,0.074,0.591,0.107,0.846c0.099,0.51-0.027,0.915-0.024,1.188c-0.022,0.274-0.036,0.42-0.036,0.42L16.021,8.484z     M15.541,8.352c0,0,0.13-1.099,0.02-1.604c-0.041-0.252-0.063-0.536-0.129-0.834c-0.102-0.291-0.209-0.604-0.326-0.94    c-0.146-0.321-0.356-0.637-0.549-0.982c-0.229-0.322-0.523-0.621-0.805-0.951c-0.675-0.552-1.429-1.104-2.417-1.337    c-0.943-0.325-2.02-0.258-3.054-0.035C7.245,1.954,6.292,2.563,5.514,3.36C4.769,4.208,4.309,5.224,4.072,6.357    C3.944,7.477,4.066,8.613,4.504,9.628c0.484,0.983,1.184,1.836,2.05,2.413c0.208,0.152,0.443,0.23,0.658,0.352    c0.213,0.116,0.427,0.229,0.7,0.281c0.255,0.067,0.508,0.135,0.758,0.202c0.173,0.031,0.462,0.029,0.682,0.047    c0.946,0.086,1.815-0.196,2.571-0.487c0.347-0.217,0.697-0.395,0.995-0.612c0.271-0.256,0.553-0.472,0.773-0.715    c0.199-0.263,0.387-0.509,0.56-0.734c0.142-0.239,0.233-0.471,0.34-0.669c0.052-0.102,0.101-0.194,0.146-0.284    c0.039-0.1,0.061-0.211,0.089-0.305c0.051-0.192,0.094-0.356,0.129-0.489c0.067-0.267,0.106-0.407,0.106-0.407L15.541,8.352z');
        borderPath.setAttribute('fill', setCtrl.btnBgBottom);
        circleBorder.appendChild(borderPath);

        info.appendChild(infoBG);
        self.appendChild(info);

        infoBG.addEventListener('click', function(){
            if(showInfo){
                infoHide();
            }else{
                infoShow();
            }
        }, false);
    }

    function mkFolderBG(){

        // **  Folder SVG  ** //
        folderBG = document.createElementNS(xmlns, 'svg');
        folderBG.setAttribute('baseProfile', 'tiny');
        folderBG.setAttribute('xlmns', xmlns);
        folderBG.setAttribute('viewBox', '0 0 '+ '40' +' '+ '40');
        folderBG.setAttribute('preserveAspectRatio', 'none');
        folderBG.setAttribute('height', objct.settings.thumb_size[0] * 1.25 + 'px');
        folderBG.setAttribute('width', objct.settings.thumb_size[0] * 1.25 + 'px');
        folderBG.setAttribute('x', '0');
        folderBG.setAttribute('y', '0');
        folderBG.style.display = 'block';

        let tabG = document.createElementNS(xmlns, 'g');
        tabG.setAttribute('fill', setCtrl.btnBgBottom);
        folderBG.appendChild(tabG);

        let folderBody = document.createElementNS(xmlns, 'path');
        folderBody.setAttribute('id', 'folderBody');
        folderBody.setAttribute('fill', '#00ADEE');
        folderBody.setAttribute('fill', setCtrl.folder.folderBG);

        folderBody.setAttribute('fill-rule', 'evenodd');
        folderBody.setAttribute('d', 'M18.601,35.785c-5.4,0-10.8,0-16.2-0.001c-1.267,0-1.398-0.116-1.398-1.341\nc-0.003-10.533-0.003-21.067,0-31.601c0-1.307,0.157-1.458,1.413-1.458c4.167-0.001,8.333,0.024,12.5-0.018\nc0.99-0.01,1.305,0.407,1.29,1.305c-0.02,1.2,0.045,2.403-0.022,3.599c-0.043,0.756,0.188,0.937,0.934,0.933\nc5.9-0.032,11.8-0.019,17.7-0.018c1.252,0,1.382,0.12,1.383,1.358c0.003,8.633,0.003,17.266,0,25.9\nc-0.001,1.226-0.132,1.342-1.398,1.342C29.4,35.785,24.001,35.785,18.601,35.785z');
        tabG.appendChild(folderBody);

        let tabShadow = document.createElementNS(xmlns, 'path');
        tabShadow.setAttribute('fill', setCtrl.folder.tabShadow);
        tabShadow.setAttribute('d', 'M15.601,2.983c0.439,1.293,0.166,2.622,0.147,3.93\nc-0.008,0.532-0.605,0.662-1.085,0.663c-4.094,0.01-8.188,0.007-12.282,0.006c-0.654,0-0.979-0.335-0.98-0.987\nC1.4,5.597,1.371,4.597,1.41,3.6c0.033-0.847,0.53-1.396,1.372-1.402c3.894-0.031,7.789-0.013,11.683-0.014\nc0.204,0,0.4,0.02,0.537,0.199c-0.086,0.097-0.19,0.167-0.313,0.208C12.277,2.753,9.863,2.618,7.45,2.666\nc-1.553,0.03-3.107-0.077-4.655,0.066C2.436,2.858,2.187,3.109,2.007,3.436C1.781,4.534,1.785,5.629,2.012,6.722\nc0.137,0.163,0.315,0.257,0.514,0.321c4.046,0.094,8.089,0.118,12.129-0.02c0.2-0.082,0.379-0.193,0.514-0.367\nc0.318-0.759,0.096-1.554,0.156-2.329c0.028-0.357-0.025-0.721,0.079-1.074C15.444,3.145,15.51,3.055,15.601,2.983z');
        tabG.appendChild(tabShadow);

        let tabber = document.createElementNS(xmlns, 'path');
        // this is the fron tab accent collor
        tabber.setAttribute('fill', setCtrl.folder.folderTab);
        tabber.setAttribute('id', 'tabber');
        tabber.setAttribute('d', 'M14.799,2.58c0.199,0.202,0.397,0.403,0.597,0.604\nc0.169,1.267,0.167,2.533,0.001,3.799c-0.133,0.068-0.267,0.136-0.4,0.204c-0.312,0.169-0.651,0.088-0.976,0.089\nc-3.614,0.006-7.227,0.006-10.841,0c-0.325-0.001-0.664,0.081-0.976-0.09C2.037,7.179,1.885,7.144,1.797,6.981\nC1.644,5.715,1.635,4.449,1.804,3.183c0.2-0.201,0.399-0.401,0.599-0.603c0.312-0.171,0.651-0.089,0.976-0.09\nc3.481-0.007,6.963-0.007,10.444,0C14.148,2.491,14.487,2.409,14.799,2.58z');
        tabG.appendChild(tabber);

        let tabAccent_bottom = document.createElementNS(xmlns, 'path');
        tabAccent_bottom.setAttribute('fill', setCtrl.folder.tabAccent);
        tabAccent_bottom.setAttribute('d', 'M2.204,7.187c4.264,0,8.529,0,12.793,0c-0.228,0.278-0.545,0.193-0.833,0.193\nc-3.709,0.005-7.418,0.005-11.127,0C2.749,7.38,2.431,7.465,2.204,7.187z');
        tabG.appendChild(tabAccent_bottom);

        let tabAccent_top = document.createElementNS(xmlns, 'path');
        tabAccent_top.setAttribute('fill', setCtrl.folder.tabAccent);
        tabAccent_top.setAttribute('d', 'M14.799,2.58c-4.132,0-8.264,0-12.396,0c0.296-0.324,0.686-0.19,1.035-0.191\nc3.787-0.009,7.574-0.006,11.361-0.005C14.8,2.45,14.8,2.515,14.799,2.58z');
        tabG.appendChild(tabAccent_top);

        let tabAccent_left = document.createElementNS(xmlns, 'path');
        tabAccent_left.setAttribute('fill', setCtrl.folder.tabAccent);
        tabAccent_left.setAttribute('d', 'M1.804,3.183C1.802,4.449,1.8,5.715,1.797,6.981C1.484,5.715,1.472,4.449,1.804,3.183z');
        tabG.appendChild(tabAccent_left);

        let tabAccent_right = document.createElementNS(xmlns, 'path');
        tabAccent_right.setAttribute('fill', setCtrl.folder.tabAccent);
        tabAccent_right.setAttribute('d', 'M15.397,6.984c0-1.267-0.001-2.533-0.001-3.799c0.068,0,0.136,0,0.204,0\nC15.486,4.449,15.84,5.737,15.397,6.984z');
        tabG.appendChild(tabAccent_right);
    }

    function escFsEvts(doWhat){
        let fSh = ['fullscreenchange', 'mozfullscreenchange', 'MSFullscreenChange', 'webkitfullscreenchange'];
        
        if(doWhat == 'mk'){
            for(let fH in fSh){
                document.addEventListener(fSh[fH], escBtn, false);
            }
        }else{
            for(let fH in fSh){
                document.removeEventListener(fSh[fH], escBtn, false);
            }
        }
    }

    function escBtn(){
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        if(full === null || full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            exitFullScreen();
            escFsEvts('rm');
        }
    }

    function exitFullScreen(){
        let vp = '"viewport"';
        document.querySelector('meta[name='+ vp +']').content = metaScale;
        // console.log(document.querySelector('meta[name='+ vp +']').content);
        fsb.removeEventListener('click', exitFullScreen, false);
        fsb.addEventListener('click', fullScreen, false);

        if(showInfo){
            infoHide();
            setTimeout(null, 1000);
        }

        if(document.exitFullscreen){
            document.exitFullscreen();
        }else if(document.webkitExitFullscreen){
            document.webkitExitFullscreen();
        }else if(document.mozCancelFullScreen){
            document.mozCancelFullScreen();
        }else if(document.msExitFullscreen){
            document.msExitFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }

        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        
        if(full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            isFS = false;
            direction = 'zoom';
            image_getter(true);
        }
    }

    function fullScreen(){
        let vp = '"viewport"';
        // console.log(document.querySelector('meta[name='+ vp +']').content);
        // screen.orientation.lock("landscape-primary");
        document.querySelector('meta[name='+ vp +']').content = "width=device-width, height=device-height, initial-scale=1";
        fsb.removeEventListener('click', fullScreen, false);
        fsb.addEventListener('click', exitFullScreen, false);

        if(showInfo){
            let splash = document.getElementById('gallery-backsplash');
            splash.style.left = (wW - mkInt(splash.style.width)) * 0.5 + 'px';
            infoHide();
            setTimeout(null, 1000);
        }

        if(self.requestFullscreen){
          self.requestFullscreen();
        }else if(self.mozRequestFullScreen){
          self.mozRequestFullScreen();
        }else if(self.webkitRequestFullscreen){
          self.webkitRequestFullscreen();
        }else if(self.msRequestFullscreen){
            self.msRequestFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }

        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;

        if(full === undefined || Object.getPrototypeOf(full) !== Object.prototype){
            let img = stage.children[0].children[0];
            isFS = true;
            img.style.visibility = 'hidden';
            direction = 'zoom';
            image_getter(true);
            escFsEvts('mk');
        }
    }

    function image_getter(pos){
        let iDiv = stage.children[0];
        let img = iDiv.children[0];
        let tW = mkInt(stage.style.width);
        let tH = mkInt(stage.style.height);
        let imgW = iGallery.images[gIndex].width, imgH = iGallery.images[gIndex].height;

        if(isFS){
            // AJAX Request for getting/setting gallery playlist and sizes
            let vars, phpLoc,  req = new XMLHttpRequest();
            
            vars = 'functToGet=imageGet&width='+wW+'&height='+ (wH - (objct.settings.thumb_size[1]) * 1.1);
            vars += '&gallery='+escape(iKey)+'&picLoc='+escape(iGallery.images[gIndex].path);

            phpLoc = 'scripts/gallery.php';

            if(tLoc !== ''){
                let pLoc = tLoc+'/';
                phpLoc = pLoc+phpLoc;
            }
            
            req.open('POST', phpLoc, true);
            
            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            req.onreadystatechange = function(){
                if(req.readyState == 4 && req.status == 200){
                    let ret = req.responseText;
                    ret = ret.split('_&_');
                    imgW = ret[0];
                    imgH = ret[1];

                    iDiv.removeChild(img);
                    img = document.createElement('img');
                    iDiv.appendChild(img);

                    img.src = ret[2];
                    fsWidth = imgW;
                    fsHeight = imgH;

                    img.id = iKey+'_'+gIndex;
                    img.style.height = imgH + 'px';
                    img.style.width = imgW + 'px';

                    iDiv.style.width = imgW + 'px';
                    iDiv.style.height = imgH + 'px';
                    iDiv.style.top = (tH - imgH) * 0.5 + 'px';

                    if(pos !== undefined && pos !== null && pos !== false){
                        mkPos();
                    }else{

                        if(direction !== 'zoom'){
                            iDiv.style.visibility = 'visible';
                            if(direction == 'left'){
                                iDiv.style.left = -Math.abs(imgW) + 'px';
                            }
                            gsap.to(iDiv, {
                                left: (tW - imgW) * 0.5 + 'px',
                                duration: 1,
                                ease: Power2.easeIn
                            });
                        }else{
                            iDiv.style.scaleX = 0;
                            iDiv.style.scaleY = 0;
                            iDiv.style.visibility = 'visible';
                            gsap.to(iDiv, {
                                left: (tW - imgW) * 0.5 + 'px',
                                scaleX: 1,
                                scaleY: 1,
                                duration: 1,
                                ease: Power2.easeIn
                            });
                        }
                    }
                }
            };

            req.send(vars);
        
        }else{

            iDiv.removeChild(img);
            img = document.createElement('img');
            iDiv.appendChild(img);

            img.src = mkPath(iGallery.images[gIndex].path);
            img.addEventListener('load', function(){

                img.id = iKey+'_'+gIndex;
                img.style.height = imgH + 'px';
                img.style.width = imgW + 'px';

                iDiv.style.width = imgW + 'px';
                iDiv.style.height = imgH + 'px';
                iDiv.style.top = (tH - imgH) * 0.5 + 'px';

                iDiv.style.visibility = 'visible';

                if(pos !== undefined && pos !== null && pos !== false){
                    mkPos();
                }else{

                    if(direction !== 'zoom'){
                        if(direction == 'left'){
                            iDiv.style.left = -Math.abs(imgW) + 'px';
                        }
                        gsap.to(iDiv, {
                            left: (tW - imgW) * 0.5 + 'px',
                            duration: 1,
                            ease: Power2.easeIn
                        });
                    }else{
                        gsap.to(iDiv, {
                            left: (tW - imgW) * 0.5 + 'px',
                            scaleX: 1,
                            scaleY: 1,
                            duration: 1,
                            ease: Power2.easeIn
                        });
                    }
                }
            });
        }
    }

    function mkPos(){
        let plus = document.getElementById('gallery-fwdBtn');
        let minus = document.getElementById('gallery-bckBtn');
        let stage = document.getElementById('stage');
        let imgHolder = document.getElementById('gallery-imgHolder');

        let img = imgHolder.children[0];

        if(isFS){

            self.style.width = wW + 'px';
            self.style.height = wH - objct.settings.thumb_size[1] + 'px';
            
            plus.style.bottom = '0px';
            plus.style.right = (wW * 0.3) + (mkInt(plus.style.width)) + 'px';

            minus.style.bottom = '0px';
            minus.style.left = (wW * 0.3) + (mkInt(plus.style.width)) + 'px';

            fsb.style.right = mkInt(plus.style.right) + mkInt(plus.style.width) + 3 + 'px';
            fsb.style.bottom = '0px';

            info.style.right = mkInt(fsb.style.right) - 3 + 'px';
            info.style.bottom = Math.abs(mkInt(btnH) * 0.6) + 'px';

        }else{

            self.style.width = objct.settings.img_size[0] + 'px';
            self.style.height = objct.settings.img_size[1] + 'px';

            plus.style.bottom = -Math.abs(mkInt(btnH)) + 'px';
            plus.style.right = '0px';

            minus.style.bottom = -Math.abs(mkInt(btnH)) + 'px';
            minus.style.left = '0px';

            fsb.style.bottom = -Math.abs(mkInt(btnH) * 1.25) + 'px';
            fsb.style.right = mkInt(btnW) + 3 + 'px';

            info.style.right = mkInt(btnW) + 'px';
            info.style.bottom = -Math.abs(mkInt(btnH) * 0.75) + 'px';
        }

        stage.style.width = (mkInt(self.style.width)  * setCtrl.stageMultiplier) + 'px';
        stage.style.height = mkInt(self.style.height) + 'px';
        stage.style.left = '0px';
        stage.style.top =  '0px';

        imgHolder.style.top = (mkInt(stage.style.top) + (mkInt(stage.style.height) - mkInt(img.style.height)) * 0.5) + 'px';
        imgHolder.style.left =  (mkInt(stage.style.width) - mkInt(img.style.width)) * 0.5 + 'px';

        if(mkInt(stage.style.width) > mkInt(self.style.width)){
            stage.style.left = -Math.abs((mkInt(stage.style.width) - mkInt(self.style.width)) * 0.5) + 'px';

        }else{
            stage.style.left = '0px';
        }

        // cereate our thumbnails, their listeners and scrub-bar
        thumbnailer(iGallery);

        // create the gallery and picture info suff
        infoSplash();
    }

    function build(){
        let padding = 5;

        // **  Plus Button SVG's  ** //
        let plus = document.createElement('div');
        plus.id = 'gallery-fwdBtn';
        plus.style.position = 'absolute';
        plus.style.width = mkInt(btnW) + 'px';
        plus.style.height = mkInt(btnH) + 'px';
        plus.style.zIndex = mkInt(self.style.zIndex) + 5;

        let plusHolder = document.createElement('div');
        plusHolder.style.width = btnW;
        plusHolder.style.height = btnH;
        plusHolder.style.position = 'absolute';
        plusHolder.style.visibility = 'visible';
        plusHolder.style.top = '0px';
        plusHolder.style.left = '0px';

        btnS.push(plusHolder);

        let plusBtn = document.createElementNS(xmlns, 'svg');
        plusBtn.setAttribute('xlmns', xmlns);
        plusBtn.setAttribute('viewBox', '0 0 20 15');
        plusBtn.setAttribute('preserveAspectRatio', 'none');
        plusBtn.setAttribute('height', mkInt(btnH));
        plusBtn.setAttribute('width', mkInt(btnW));
        plusBtn.setAttribute('x', '0px');
        plusBtn.setAttribute('y', '0px');
        plusBtn.style.position = 'absolute';

        let plus_btn = document.createElementNS(xmlns, 'path');
        plus_btn.setAttribute('d', 'M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z');
        plus_btn.setAttribute('fill', setCtrl.btnColUp);
        plus_btn.setAttribute('id', 'gallery-plus_btn');
        plus_btn.style.zIndex = mkInt(self.style.zIndex) + 5;

        let plusTop = document.createElement('div');
        plusTop.id = 'gallery-plusTop';
        plusTop.style.height = btnH;
        plusTop.style.width = btnW;
        plusTop.style.position = 'absolute';
        plusTop.style.zIndex = mkInt(self.style.zIndex) + 6;
        plusTop.style.top = '0px';
        plusTop.style.left = '0px';
        plusTop.style.padding = '5px';

        plusHolder.appendChild(plusTop);
        plusBtn.appendChild(plus_btn);
        plusHolder.appendChild(plusBtn);
        plusHolder.appendChild(btn_bg);

        plus.appendChild(plusHolder);
        self.appendChild(plus);

        // ** Minus Button SVG's ** //
        let minus_bg = btn_bg.cloneNode(true);

        let minus = document.createElement('div');
        minus.id = 'gallery-bckBtn';
        minus.style.position = 'absolute';
        minus.style.width = mkInt(btnW) + 'px';
        minus.style.height = mkInt(btnH) + 'px';
        minus.style.zIndex = mkInt(self.style.zIndex) + 6;
        minus.style.transform="scale(-1,1)";

        let minusHolder = plusHolder.cloneNode(true);

        // Since we cloned our node we have to find some elements id's and make them unique
        // so our animations will work
        let children_ = minusHolder.getElementsByTagName('*');
        for(let node in children_){
            if(children_[node].id === 'gallery-plusTop'){
                children_[node].id = 'gallery-minusTop';
            }

            if(children_[node].id === 'gallery-plus_btn'){
                children_[node].id = 'gallery-minus_btn';
            }
        }

        minus.appendChild(minusHolder);
        btnS.push(minusHolder);
        self.appendChild(minus);

        // **  Fullscreen Button SVG's  ** //
        fsb = document.createElement('div');
        fsb.id = 'gallery-FSbtn';
        fsb.style.zIndex = minus.style.zIndex;
        let mtlp = 0.65;
        fsb.style.width = mkInt(btnH) * mtlp + 'px';
        fsb.style.height = mkInt(btnH) * mtlp + 'px';
        fsb.style.position = 'absolute';
        fsb.style.overflow = 'hidden';

        let fsb_bg = btn_bg.cloneNode(true);
        fsb_bg.setAttribute('viewBox', '0 0 20 20');
        fsb_bg.setAttribute('height', mkInt(btnH) * mtlp);
        fsb_bg.setAttribute('width', mkInt(btnH) * mtlp);

        let fsbBtn = document.createElementNS(xmlns, 'svg');
        fsbBtn.setAttribute('xlmns', xmlns);
        fsbBtn.setAttribute('viewBox', '0 0 20 20');
        fsbBtn.setAttribute('preserveAspectRatio', 'none');
        fsbBtn.setAttribute('height', mkInt(btnH) * mtlp);
        fsbBtn.setAttribute('width', mkInt(btnH) * mtlp);
        fsbBtn.setAttribute('x', '0px');
        fsbBtn.setAttribute('y', '0px');
        fsbBtn.style.position = 'absolute';

        let fsb_btn = document.createElementNS(xmlns, 'path');
        fsb_btn.setAttribute('d', 'M14.327 11.612h.63L14.95 4h-.63v.56h-.908v-.586H6.608v.004h-.02v.582h-.913v-.554h-.633l.006 7.61h.63v-.542h.915v.568h6.817v-.568h.914l.003.538zM7.54 10.576L7.538 5.04h4.923l.008 5.536H7.54zm-1.863-2.52h.915v1.03H5.68v-1.03zm.914-.48h-.91v-1.03h.915v1.03zm6.82-1.03h.916v1.03h-.913l-.002-1.03zm0 1.51h.917v1.03h-.914v-1.03zm.91-3.017l.005 1.03h-.915V5.04h.912zm-7.73 0v1.03h-.91V5.04h.913zm-.91 5.55V9.56h.916v1.03H5.68zm7.735 0v-.018l-.002-1.012h.916v1.03h-.914z');
        fsb_btn.setAttribute('fill', setCtrl.btnColUp);
        fsb_btn.setAttribute('id', 'gallery-fScreen_btn');

        let fScreenTop = document.createElement('div');
        fScreenTop.id = 'gallery-fScreenTop';
        fScreenTop.style.height = mkInt(btnH) * mtlp + 'px';
        fScreenTop.style.width = mkInt(btnH) * mtlp + 'px';
        fScreenTop.style.position = 'absolute';
        fScreenTop.style.zIndex = mkInt(self.style.zIndex) + 6;

        fsb.appendChild(fScreenTop);
        fsbBtn.appendChild(fsb_btn);
        fsb.appendChild(fsbBtn);
        fsb.appendChild(fsb_bg);

        btnS.push(fsb);
        self.appendChild(fsb);

        // **  Stopwatch Button SVG's  ** //
        // not made yet? maybe not? maybe?

        if(setCtrl.startGallery == ''){
            iGallery = Object.values(objct.gallerys)[0];
        }else{
            iGallery = objct.gallerys[setCtrl.startGallery];
        }

        iKey = Object.keys(objct.gallerys).find(k=>objct.gallerys[k]===iGallery);

        // create the slideShow image

        let obj = iGallery.images;
        let img = document.createElement('img');
        img.style.visibility = 'hidden';
        img.style.positiion = 'absolute';
        img.style.width = obj[0].width + 'px';
        img.style.height = obj[0].height + 'px';
        img.style.left = '0px';
        img.style.top = '0px';

        img.addEventListener('load', function(){
            img.style.visibility = 'visible';
            gsap.from(img, {scaleX: 0,
                            scaleY: 0,
                            opacity: 0,
                            ease: Power2.easeOut,
                            duration: 0.5});
        });

        let stage = document.createElement('div');
        stage.id = 'stage';
        stage.style.zIndex = mkInt(self.style.zIndex) + 1;
        stage.style.position = 'absolute';
        stage.style.overflow = 'hidden';

        let imgHolder = document.createElement('div');
        imgHolder.id = 'gallery-imgHolder';
        imgHolder.style.position = 'absolute';
        imgHolder.style.width = img.style.width;
        imgHolder.style.height = img.style.height;
        imgHolder.style.zIndex = mkInt(self.style.zIndex) + 5;
        imgHolder.appendChild(img);

        img.style.zIndex = mkInt(imgHolder.style.zIndex) + 1;

        gIndex = 0;
        img.id = iKey+'_'+gIndex;

        img.src = mkPath(obj[0].path);
        stage.appendChild(imgHolder);
        self.appendChild(stage);

        mkPos();
    }

    // =========  end of helper functions ============ //
    
    if(self === undefined || self === null || self === ""){
        self = document.getElementById('gallery');
    }
    if(configFile === undefined || configFile === null || configFile === ""){
        loc = 'scripts/gallery.json';
    }else{
        loc = configFile;
    }
    if(verbose === undefined || verbose === null || verbose !== true){
        verbose = false;
    }

    let xmlns = 'http://www.w3.org/2000/svg';

    if(setCtrl === undefined || setCtrl === null || Object.getPrototypeOf(setCtrl) !== Object.prototype){
        console.log('No settings.');
        setCtrl = {'topDir': '',
                   'galleryOrder': false,
                   'startGallery': false,
                   'stageMultiplier': 1.0,
                   // 'buttonW': mkInt(self.style.height) * 0.10 + 10 + 'px',
                   // 'buttonH': mkInt(self.style.height) * 0.10 + 'px',
                   'btnColUp': '#458CCB',
                   'btnColOver': '#72ABDD',
                   'btnBgTop': '#181EbF',
                   'btnBgTopAlpha': 1.0,
                   'btnBgBottom': '#10100A',
                   'btnBgBottomAlpha': 1.0,
                   'nowPlayColor': '#91C7F6',
                   'folder': {'folderBG': '#00ADEE',
                              'folderTab': '#00BEF2',
                              'tabShadow': '#00A4E0',
                              'tabAccent': '#00A4E2'}};
    }else{
        // console.log('settings');
        if(setCtrl.topDir === undefined || typeof setCtrl.topDir !== 'string'){
            setCtrl.topDir = '';
            mkLog('Setting-topDir ( * Default * )');
        }
        else{
            mkLog('Setting-topDir (Type expected, String:URL): '+setCtrl.topDir);
        }
        if(setCtrl.galleryOrder === undefined || Object.prototype.toString.call(setCtrl.galleryOrder) !== '[object Array]'){
            setCtrl.galleryOrder = false;
            mkLog('Setting-galleryOrder ( * Default * )');
        }else{
            mkLog('Setting-galleryOrder (Type expected, Array): '+setCtrl.galleryOrder);
        }
        if(setCtrl.startGallery === undefined || typeof setCtrl.startGallery !== 'string'){
            setCtrl.startGallery = '';
            mkLog('Setting-startGallery ( * Default * )');
        }
        else{
            mkLog('Setting-startGallery (Type expected, String:URL): '+setCtrl.startGallery);
        }
        if(setCtrl.stageMultiplier === undefined || setCtrl.stageMultiplier === null){
            setCtrl.stageMultiplier = 1.0;
            mkLog('Setting-stageMultiplier ( * Default * )');
        }else{
            if(typeof setCtrl.stageMultiplier === 'number' && setCtrl.stageMultiplier %1 === 0){
                setCtrl.stageMultiplier = 1.0;
                mkLog('Setting-stageMultiplier (Type expected, Integer 0.0-1.0): '+setCtrl.stageMultiplier);
            }
        }

        // ***  KEEP THIS! WE MIGHT WANT TO GIVE THRE USER ABILITY TO SET CUSTOM BUTTON SIZE  *** //
        // if(setCtrl.buttonH === undefined || typeof setCtrl.buttonH !== 'string'){
        //     setCtrl.buttonH = mkInt(gallery.style.height) * 0.10 + 'px';
        //     mkLog('Setting-buttonH ( * Default * )');
        // }else{
        //     mkLog('Setting-buttonH (Type expected, String-number in pixels): '+setCtrl.buttonH);
        // }
        // if(setCtrl.buttonW === undefined || typeof setCtrl.buttonW !== 'string'){
        //     setCtrl.buttonW = mkInt(gallery.style.height) * 0.10 + 10 + 'px';
        //     mkLog('Setting-buttonW ( * Default * )');
        // }else{
        //     mkLog('Setting-buttonW (Type expected, String-number in pixels): '+setCtrl.buttonW);
        // }

        if(setCtrl.btnColUp === undefined || typeof setCtrl.btnColUp !== 'string'){
            setCtrl.btnColUp = '#458CCB';
            mkLog('Setting-btnColUp ( * Default * )');
        }else{
            mkLog('Setting-btnColUp (Type expected, Hexadecimal Color String): '+setCtrl.btnColUp);
        }
        if(setCtrl.btnColOver === undefined || typeof setCtrl.btnColOver !== 'string'){
            setCtrl.btnColOver = '#72ABDD';
            mkLog('Setting-btnColOver ( * Default * )');
        }else{
            mkLog('Setting-btnColOver (Type expected, Hexadecimal Color String): '+setCtrl.btnColOver);
        }
        if(setCtrl.btnBgTop === undefined || typeof setCtrl.btnBgTop !== 'string'){
            setCtrl.btnBgTop = '#181EbF';
            mkLog('Setting-btnBgTop ( * Default * )');
        }else{
            mkLog('Setting-btnBgTop (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTop);
        }
        // if(setCtrl.btnBgTopOver === undefined || typeof setCtrl.btnBgTopOver !== 'string'){
        //     setCtrl.btnBgTopOver = '#404249'
        //     mkLog('Setting-btnBgTopOver ( * Default * )');
        // }else{
        //     mkLog('Setting-btnBgTopOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTopOver);
        // }
        if(setCtrl.btnBgTopAlpha === undefined ||  setCtrl.btnBgTopAlpha === null || setCtrl.btnBgTopAlpha %1 === 0){
            setCtrl.btnBgTopAlpha = 1.0;
            mkLog('Setting-btnBgTopAlpha ( * Default * )');
        }else{
            mkLog('Setting-btnBgTopAlpha (Type expected, Integer 0.0-1.0): '+setCtrl.btnBgTopAlpha);
        }
        if(setCtrl.btnBgBottom === undefined || typeof setCtrl.btnBgBottom !== 'string'){
            setCtrl.btnBgBottom = '#10100A';
            mkLog('Setting-btnBgBottom ( * Default * )');
        }else{
            mkLog('Setting-btnBgBottom (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottom);
        }
        // if(setCtrl.btnBgBottomOver === undefined || typeof setCtrl.btnBgBottomOver !== 'string'){
        //     setCtrl.btnBgBottomOver = '#2C365C';
        //     mkLog('Setting-btnBgBottomOver ( * Default * )');
        // }else{
        //     mkLog('Setting-btnBgBottomOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottomOver);
        // }
        if(setCtrl.btnBgBottomAlpha === undefined || setCtrl.btnBgBottomAlpha === null || setCtrl.btnBgBottomAlpha %1 === 0){

            // console.log(setCtrl.btnBgBottomAlpha);
            // setCtrl.btnBgBottomAlpha = setCtrl.btnBgBottomAlpha;

            // setCtrl.btnBgBottomAlpha = 1.0;
            mkLog('Setting-btnBgBottomAlpha ( * Default * )');
        }else{
            mkLog('Setting-btnBgBottomAlpha (Type expected, Float 0.0-1.0): '+setCtrl.btnBgBottomAlpha);
        }
        if(setCtrl.nowPlayColor === undefined || typeof setCtrl.nowPlayColor !== 'string'){
            // Now playing thumbnail text`
            setCtrl.nowPlayColor = '#458CCB';
            mkLog('Setting-nowPlayColor ( * Default * )');
        }else{
            mkLog('Setting-nowPlayColor (Type expected, Hexadecimal Color String): '+setCtrl.nowPlayColor);
        }
        if(setCtrl.folder === undefined || setCtrl.folder === null){
            setCtrl.folder = {'folderBG': setCtrl.btnBgTop,
                              'folderTab': setCtrl.btnColUp,
                              'tabShadow': setCtrl.btnBgBottom,
                              'tabAccent': setCtrl.btnOver};

        }else{
            if(Object.getPrototypeOf(setCtrl.folder) !== Object.prototype){
                if(typeof setCtrl.folder == 'string' && setCtrl.folder == 'blue' || setCtrl.folder == 'Blue'){
                    setCtrl.folder = {'folderBG': '#00ADEE',
                                      'folderTab': '#00BEF2',
                                      'tabShadow': '#00A4E0',
                                      'tabAccent': '#00A4E2'};
                }else{
                    // user passed unintended data. Set todefaults
                    setCtrl.folder = {'folderBG': setCtrl.btnBgTop,
                                      'folderTab': setCtrl.btnColUp,
                                      'tabShadow': setCtrl.btnBgBottom,
                                      'tabAccent': setCtrl.btnOver};
                }
            }else{
                // do object checker here
                if(setCtrl.folder.folderBG !== undefined || setCtrl.folder.folderBG !== null || typeof setCtrl.folder.folderBG !== 'string'){
                    setCtrl.folder.folderBG = setCtrl.btnBgTop;
                }else{
                    mkLog('Setting-folderBG (Type expected, Hexadecimal Color String): '+setCtrl.folder.folderBG);
                }

                if(setCtrl.folder.folderTab !== undefined || setCtrl.folder.folderTab !== null || typeof setCtrl.folder.folderTab !== 'string'){
                    setCtrl.folder.folderTab = setCtrl.btnColUp;
                }else{
                    mkLog('Setting-folderTab (Type expected, Hexadecimal Color String): '+setCtrl.folder.folderTab);
                }

                if(setCtrl.folder.tabShadow !== undefined || setCtrl.folder.tabShadow !== null || typeof setCtrl.folder.tabShadow !== 'string'){
                    setCtrl.folder.tabShadow = setCtrl.btnBgBottom;
                }else{
                    mkLog('Setting-tabShadow (Type expected, Hexadecimal Color String): '+setCtrl.folder.tabShadow);
                }

                if(setCtrl.folder.tabAccent !== undefined || setCtrl.folder.tabAccent !== null || typeof setCtrl.folder.tabAccent !== 'string'){
                    setCtrl.folder.tabAccent = setCtrl.btnBgBottom;
                }else{
                    mkLog('Setting-tabAccent (Type expected, Hexadecimal Color String): '+setCtrl.folder.tabAccent);
                }
            }
        }
    }

    //  ** Start building our playlist **  //

    // AJAX Request for getting/setting gallery playlist and sizes
    let req = new XMLHttpRequest();
    let vars;
    if(loc.includes('.json')){
        req.open('GET', loc, true);
    }
    else{

        if(!setCtrl.playlist){
            vars = 'functToGet=mkGallery';
        }else{
            vars = 'functToGet=imageSort&playlist='+setCtrl.playlist.join(',');
        }
        req.open('POST', loc, true);
    }

    objct = null;
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function(){
        if(req.readyState == 4 && req.status == 200){
            let ret = req.responseText;
            objct = JSON.parse(ret);
            tLoc = getTopDir();

            self.style.width = objct.settings.img_size[0];
            self.style.height = objct.settings.img_size[1];

            if(self.style.zIndex == ''){
                self.style.zIndex = mkInt(self.parentNode.style.zIndex) + 1;
            }

            btnH = objct.settings.thumb_size[1] * 0.6 + 'px';
            btnW = objct.settings.thumb_size[0] * 0.3 + 'px';

            // ** Build all the background parts ** \\
            // create the button background SVG
            mkButtonBG();

            // create the info button background SVG
            mkInfoBG();

            // create the folder button background SVG
            mkFolderBG();

            // here we build our gallery out of it's parts.
            build();

            // create the button mouseover:out listeners and events
            for(let btn in btnS){
                let btnDiv = btnS[btn];
                btnDiv.addEventListener('mouseover', btnOver, false);
                btnDiv.addEventListener('mouseout', btnOut, false);
                if(btnDiv.id !== 'gallery-FSbtn'){
                    btnDiv.addEventListener('click', imgChanger, false);
                }else{
                    fsb.addEventListener('click', fullScreen, false);
                }
            }

            // create our info splash container and elements then hide it initially
            infoSplash();
            infoHide();
        }
    };

    if(loc.includes('.json')){
        req.send(null);
    }
    else{
        req.send(vars);
    }
}